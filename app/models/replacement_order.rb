require 'aasm'

# model
class ReplacementOrder < ApplicationRecord
  include AASM

  belongs_to :owner,    class_name: 'User'
  belongs_to :indicate, class_name: 'User'

  aasm column: 'state' do
    after_all_transitions :log_status_change

    state :init, initial: true
    state :awaiting_review
    state :being_reviewed
    state :awaiting_repair
    state :accepted
    state :rejected

    event :submit do
      transitions from: :init, to: :awaiting_review
    end

    event :review do
      transitions from: :awaiting_review, to: :being_reviewed
    end

    event :accept do
      transitions from: :being_reviewed, to: :accepted
    end

    event :reject do
      transitions from: :being_reviewed, to: :rejected
    end

    event :repair do
      transitions from: :being_reviewed, to: :awaiting_repair
    end

    event :repaired do
      transitions from: :awaiting_repair, to: :awaiting_review
    end
  end

  private

  def log_status_change
    puts "Changed from #{aasm.from_state}
      to #{aasm.to_state}
      (event: #{aasm.current_event})"
  end
end
