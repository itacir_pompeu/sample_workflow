# User
class User < ApplicationRecord
  hash_owner = { class_name: 'ReplacementOrder', foreign_key: 'owner_id' }
  has_many :owner_replacement_order, hash_owner

  hash_idicate = { class_name: 'ReplacementOrder', foreign_key: 'indicate_id' }
  has_many :indicate_replacement_order, hash_idicate

  def authenticate(password)
    self.password == password
  end

  def admin?
    cargo == 'admin'
  end
end
