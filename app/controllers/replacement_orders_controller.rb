# ReplacementOrdersController
class ReplacementOrdersController < ApplicationController
  only = %i[show edit update destroy review accept reject]
  before_action :set_replacement_order, only: only
  before_action :authorize

  # GET /replacement_orders
  # GET /replacement_orders.json
  def index
    @replacement_orders = ReplacementOrder.order :end_date
  end

  # GET /replacement_orders/1
  # GET /replacement_orders/1.json
  def show; end

  # GET /replacement_orders/new
  def new
    @replacement_order = ReplacementOrder.new
  end

  def review
    @replacement_order.review!
    handler('Revisado com sucesso.')
  end

  def accept
    @replacement_order.accept!
    handler('Pedido Aceito.')
  end

  def reject
    @replacement_order.reject!
    handler('Pedido rejeitado.')
  end

  # GET /replacement_orders/1/edit
  def edit; end

  # POST /replacement_orders
  # POST /replacement_orders.json
  def create
    @replacement_order = ReplacementOrder.new(replacement_order_params)
    msg = 'Replacement order was successfully created.'

    respond_to do |format|
      if @replacement_order.submit!
        format.html { redirect_to replacement_orders_url, notice: msg }
      else
        format.html { render :new }
      end
    end
  end

  private

  def handler(msg)
    respond_to do |format|
      format.html { redirect_to replacement_orders_url, notice: msg }
    end
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_replacement_order
    @replacement_order = ReplacementOrder.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through.
  def replacement_order_params
    permissive = %i[start_date end_date comments owner_id
                    indicate_id comments_indicate]

    params.require(:replacement_order).permit(permissive)
  end
end
