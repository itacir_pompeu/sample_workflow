json.extract! replacement_order, :id, :start_date, :end_date, :comments, :has_one, :has_one, :created_at, :updated_at
json.url replacement_order_url(replacement_order, format: :json)
