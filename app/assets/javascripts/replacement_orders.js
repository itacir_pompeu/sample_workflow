// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://coffeescript.org/
$(function () {

  const start_date = $('#replacement_order_start_date')
  const end_date = $('#replacement_order_end_date')

  if (start_date.val() && end_date.val()) {
    set_diff_days(end_date, start_date) 
  }

  end_date.change(() => {
    if (start_date.val()) {
      set_diff_days(end_date, start_date)
    }
  })

  function set_diff_days (end_date, start_date) {
    const diff_days = moment(end_date.val()).diff(moment(start_date.val()), 'days')
    $('#diff_day').val(diff_days > 0 ? diff_days : 0);
  }
})
