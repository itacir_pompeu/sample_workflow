module ReplacementOrdersHelper
  def awaiting_review?(replacement_order)
    replacement_order.state == 'awaiting_review' 
  end

  def being_review?(replacement_order)
    replacement_order.state == 'being_reviewed'
  end
end
