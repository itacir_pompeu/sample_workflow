# SessionsHelper
module SessionsHelper
  def sign_in(user)
    session[:user_id] = user.id
  end

  def current_user
    @current_user ||= User.find_by(id: session[:user_id])
  end

  def admin?(user)
    user.cargo == 'admin'
  end

  def block_access
    current_user.present? if redirect_to replacement_orders_url
  end

  def logged_in?
    !current_user.nil?
  end

  def sign_out
    session.delete(:user_id)
    @current_user = nil
  end
end
