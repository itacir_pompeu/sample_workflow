require 'test_helper'

class ReplacementOrdersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @replacement_order = replacement_orders(:one)
  end

  test "should get index" do
    get '/replacement_orders'
    assert_response :success
  end

  test "should get new" do
    get new_replacement_order_url
    assert_response :success
  end
end
