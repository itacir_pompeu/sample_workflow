## Deployment instructions

1. Clone the repository.
2. Install gems with `bundle install`.
3. Migrate the database with `rails db:migrate` or `rake db:migrate`.
4. Start the application with `rails s`.

# Workflow
1. use aasm for machine state

# postrgres docker

```sh
docker run -it --rm --link postgres:postgres postgres psql -h postgres -U postgres
```

```plsql
\c rails_state_machine_development;
```
