class CreateReplacementOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :replacement_orders do |t|
      t.datetime :start_date
      t.datetime :end_date
      t.text     :comments
      t.text     :comments_indicate
      t.string   :state
      t.references :owner
      t.references :indicate

      t.timestamps
    end
  end
end
