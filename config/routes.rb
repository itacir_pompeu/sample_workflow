Rails.application.routes.draw do
  resources :replacement_orders do
    member do
      get 'review', action: :review
      get 'accept', action: :accept
      get 'reject', action: :reject
    end
  end

  get    'sessions/new'
  get    'sign_in'   => 'sessions#new'
  post   'sign_in'   => 'sessions#create'
  delete 'sign_out'  => 'sessions#destroy'

  root 'sessions#new'
end
